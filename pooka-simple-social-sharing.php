<?php
/*
Plugin Name: Pooka&co Simple Social Sharing
Plugin URI: https://www.pooka.co
Description: Tired of social share plugins adding tracking and advertising cookies to your users' browsers and slowing your site down? The Pooka to the rescue!
Version: 1.3
Author: Marcus J Wilson
*/

/* Add a Social Sharing menu item to WordPress Admin */
function social_share_menu_item() {
  add_submenu_page("options-general.php", "Social Sharing", "Social Sharing", "manage_options", "social-share", "social_share_page");
}
add_action("admin_menu", "social_share_menu_item");

/* Set up content for the Social Sharing Admin page */
function social_share_page() {
   ?>
      <div class="wrap">
         <h1>Social Sharing Options</h1>

         <form method="post" action="options.php">
            <?php
               settings_fields("social_share_config_section");

               do_settings_sections("social-share");

               submit_button();
            ?>
         </form>
      </div>
   <?php
}

/* Create the Social Sharing Settings Section */
function social_share_settings() {
    add_settings_section("social_share_config_section", "", null, "social-share");

    add_settings_field("social-share-text", "Share text", "social_share_text_input", "social-share", "social_share_config_section");
    add_settings_field("social-share-facebook", "Display Facebook share button?", "social_share_facebook_checkbox", "social-share", "social_share_config_section");
    add_settings_field("social-share-twitter", "Display Twitter share button?", "social_share_twitter_checkbox", "social-share", "social_share_config_section");
    add_settings_field("social-share-twitter-account", "Twitter account", "social_share_twitter_account_input", "social-share", "social_share_config_section");
    add_settings_field("social-share-linkedin", "Display LinkedIn share button?", "social_share_linkedin_checkbox", "social-share", "social_share_config_section");
    add_settings_field("social-share-pinterest", "Display Pinterest share button?", "social_share_pinterest_checkbox", "social-share", "social_share_config_section");
    add_settings_field("social-share-email", "Display Email share button?", "social_share_email_checkbox", "social-share", "social_share_config_section");
    add_settings_field("social-share-email-subject", "Email Subject", "social_share_email_subject_input", "social-share", "social_share_config_section");
    add_settings_field("social-share-email-body", "Email Body", "social_share_email_body_input", "social-share", "social_share_config_section");

    register_setting("social_share_config_section", "social-share-text");
    register_setting("social_share_config_section", "social-share-facebook");
    register_setting("social_share_config_section", "social-share-twitter");
    register_setting("social_share_config_section", "social-share-twitter-account");
    register_setting("social_share_config_section", "social-share-linkedin");
    register_setting("social_share_config_section", "social-share-pinterest");
    register_setting("social_share_config_section", "social-share-email");
    register_setting("social_share_config_section", "social-share-email-subject");
    register_setting("social_share_config_section", "social-share-email-body");
}

function social_share_text_input() {
   ?>
        <input type="text" name="social-share-text" value="<?php echo get_option('social-share-text', 'Share this'); ?>" />
   <?php
}

function social_share_facebook_checkbox() {
   ?>
        <input type="checkbox" name="social-share-facebook" value="1" <?php checked(1, get_option('social-share-facebook'), true); ?> /> Check for Yes
   <?php
}

function social_share_twitter_checkbox() {
   ?>
        <input type="checkbox" name="social-share-twitter" value="1" <?php checked(1, get_option('social-share-twitter'), true); ?> /> Check for Yes
   <?php
}

function social_share_twitter_account_input() {
   ?>
        <input type="text" name="social-share-twitter-account" value="<?php echo get_option('social-share-twitter-account'); ?>" />
   <?php
}

function social_share_linkedin_checkbox() {
   ?>
        <input type="checkbox" name="social-share-linkedin" value="1" <?php checked(1, get_option('social-share-linkedin'), true); ?> /> Check for Yes
   <?php
}

function social_share_pinterest_checkbox() {
   ?>
        <input type="checkbox" name="social-share-pinterest" value="1" <?php checked(1, get_option('social-share-pinterest'), true); ?> /> Check for Yes
   <?php
}

function social_share_email_checkbox() {
   ?>
        <input type="checkbox" name="social-share-email" value="1" <?php checked(1, get_option('social-share-email'), true); ?> /> Check for Yes
   <?php
}

function social_share_email_subject_input() {
	?>
    <input type="text" name="social-share-email-subject" value="<?php echo get_option('social-share-email-subject', 'Scottish Poetry Library link'); ?>" />
	<?php
}

function social_share_email_body_input() {
	?>
    <input type="text" name="social-share-email-body" value="<?php echo get_option('social-share-email-body', 'Check out this link from the Scottish Poetry Library website: '); ?>" />
	<?php
}

add_action("admin_init", "social_share_settings");

/* Display the Social Sharing buttons */
function add_social_share_icons($content) {

    global $post;

    /* Only apply to posts && products */
    $postType = get_post_type( $post->ID );
    if ( ($postType != 'post') && ($postType != 'resource') ) {

        return $content;

    } else {

        $html = get_social_share();

        return $content = $content . $html;
    }
}
add_filter("the_content", "add_social_share_icons");


function get_social_share() {

  global $post;

  $html = "<div class='pooka-sss-wrapper text-center d-print-none'><div class='share-on'>" . get_option('social-share-text', 'Share this') . "</div>";

  $url = get_permalink($post->ID);
  $url = esc_url($url);

  if(get_option("social-share-facebook") == 1) {
      $html = $html . "<div class='pooka-sss-box pooka-sss-facebook'><a target='_blank' href='http://www.facebook.com/sharer.php?u=" . $url . "'><i class='icon-facebook'></i><span>Facebook</span></a></div>";
  }

  if(get_option("social-share-twitter") == 1) {
      $via = get_option('social-share-twitter-account') ? '&via=' . get_option('social-share-twitter-account') : '';
      $html = $html . "<div class='pooka-sss-box pooka-sss-twitter'><a target='_blank' href='https://twitter.com/share?url=" . $url . $via . "'><i class='icon-twitter'></i><span>Twitter</span></a></div>";
  }

  if(get_option("social-share-linkedin") == 1) {
      $html = $html . "<div class='pooka-sss-box pooka-sss-linkedin'><a target='_blank' href='http://www.linkedin.com/shareArticle?url=" . $url . "'><i class='icon-linkedin'></i><span>LinkedIn</span></a></div>";
  }

  if(get_option("social-share-pinterest") == 1) {
      $html = $html . "<div class='pooka-sss-box pooka-sss-pinterest'><a target='_blank' href='https://pinterest.com/pin/create/button/?url=" . $url . "'><i class='icon-pinterest'></i><span>Pinterest</span></a></div>";
  }

  if(get_option("social-share-email") == 1) {
      $html = $html . "<div class='pooka-sss-box pooka-sss-email'><a href='mailto:name@email.com?&subject=" . get_option('social-share-email-subject', 'Scottish Poetry Library link') . "&body=" . get_option('social-share-email-body', 'Check out this link from the Scottish Poetry Library website: ') . " " . $url . "'><i class='icon-mail-alt'></i><span>Email</span></a></div>";
  }

  $html = $html . "<div class='pooka-sss-clear'></div></div>";

  return $html;
}



function the_social_share() {
  echo get_social_share();
}



/* Add some Pooka style */
function social_share_style() {
    wp_register_style("social-share-style-file", plugin_dir_url(__FILE__) . "style.css");
    wp_enqueue_style("social-share-style-file");
}
add_action("wp_enqueue_scripts", "social_share_style");